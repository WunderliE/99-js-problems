// P08 (**) Eliminate consecutive duplicates of list elements.

var cars = ["Ford", "Ford", "Ford", "Mercedes", "Opel", "Opel", "Opel", "Chevrolet"];
function removeDuplicateUsingSet(cars){
    let unique_array = Array.from(new Set(cars))
    return unique_array
}

console.log(removeDuplicateUsingSet(cars));
