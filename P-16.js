// P16  (**) Drop every Nth element from a list.

const months = ['Jan', 'March', 'April', 'June'];
months.splice(4, 1);
console.log(months);