P21 (*) Insert an element at a given position into an array.



function insertAt(array, index, ...elementsArray) {
    array.splice(index, 0, ...elements);
}

var num = [1,2,3,6,7,8];
/*
    arguments 
 * 1. source array - num
 * 2. index to insert - 3
 * 3. remaining are elements to insert
*/
insertAt(num, 3, 4, 5); // [1,2,3,4,5,6,7,8]