// P06 (*) Find out whether a list is a palindrome.

let palindromeArray = (arr) => {
     //initialize to true
     let isPalindrome = true;
     
     //loop through half length of the array
     for(let i = 0; i < arr.length / 2; i++) {

         //check if first half is equal to the second half
         if(arr[i] !== arr[arr.length - i - 1]){
           isPalindrome = false; 
           break;
         }
     }
     
     return isPalindrome;
}

console.log(palindromeArray([1,2,2,1]));
console.log(palindromeArray([1,2,2,2]));