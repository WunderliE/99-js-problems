// P15 (**) Duplicate the elements of a list a given number of times.

var numbers = ["1", "2", "3", "4", "5"];
var times = 4;
function repeat(numbers, times) {
    return new Array(times).fill(numbers);
}
