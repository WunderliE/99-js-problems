P20 (*) Remove the Kth element from an array.

// Find the index of the element and then remove it

var array = [2, 5, 9];

var index = array.indexOf(5);
if (index > -1) {
  array.splice(index, 1);
}
// array = [2, 9]
console.log(array); 


// A second solution, which removes the given element, but does not effect the length of the array.

var ar = [1, 2, 3, 4, 5, 6];
delete ar[4]; // delete element with index 4
console.log( ar ); // [1, 2, 3, 4, undefined, 6]
alert( ar ); // 1,2,3,4,,6