P18 (**) Extract a slice from an array.

const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];

const part = (animals.slice(0, 2));
console.log (part);