(*) Split an array into two parts.

const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];

const part1 = (animals.slice(0, 2));
const part2 = (animals.slice(2, 5));
console.log (part1);
console.log (part2);
