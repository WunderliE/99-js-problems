// P10 (*) Run-length encoding of a list.

function RunLength(str) { 
  var newStr="";
  var count=1;
  for (var i=0; i<str.length; i++) {
    if (str[i]===str[i+1]) {
      count += 1;
    } else {
      newStr += count + str[i];
      count =1;
    }
  }
  return newStr; 
}
   
RunLength("wwwbbbwaacdefgxhhhgjjiirrrtvkswbn");